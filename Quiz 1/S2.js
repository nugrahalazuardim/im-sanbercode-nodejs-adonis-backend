function hitungVokal(str) {
  var vokal = 'aiueo';
  var hitung = 0;

  for (var h = 0; h < str.length; h++) {
    if (vokal.indexOf(str.toLowerCase()[h]) !== -1) {
      hitung += 1;
    }
  }
  return hitung;
}

console.log(hitungVokal('Adonis'));
console.log(hitungVokal('Error'));
console.log(hitungVokal('Eureka'));
console.log(hitungVokal('Rsch'));
