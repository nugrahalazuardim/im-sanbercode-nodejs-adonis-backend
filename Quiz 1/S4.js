function graduate(arr) {
  var lulus = [];
  var peserta = {};

  for (var d = 0; d < arr.length; d++) {
    var data = arr[d];
    if (data.score < 60) {
      peserta[data.class] = {
        name: data.name,
        score: data.score,
        grade: 'participated',
      };
      // console.log('particpated');
    } else if (data.score >= 60 && data.score <= 85) {
      peserta[data.class] = {
        name: data.name,
        score: data.score,
        grade: 'completed',
      };
      // console.log('completed');
    } else if (data.score > 85) {
      peserta[data.class] = {
        name: data.name,
        score: data.score,
        grade: 'mastered',
      };
      // console.log('mastered');
    }
    lulus.push(peserta);
  }
  return lulus;
}

var arr = [
  { name: 'Ahmad', score: 80, class: 'Laravel' },
  { name: 'Regi', score: 86, class: 'Vuejs' },
  { name: 'Robert', score: 59, class: 'Laravel' },
  { name: 'Bondra', score: 81, class: 'Reactjs' },
];

console.log(graduate(arr));
