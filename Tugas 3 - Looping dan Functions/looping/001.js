var a = 2;
console.log('LOOPING PERTAMA');
while (a <= 20) {
  console.log(`${a} - I love coding`);
  a += 2;
}

var b = 20;
console.log('LOOPING KEDUA');
while (b >= 2) {
  console.log(`${b} - I will become a mobile developer`);
  b -= 2;
}
