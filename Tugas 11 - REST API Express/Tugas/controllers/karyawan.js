const fs = require('fs');
const fsPromises = require('fs/promises');
const path = 'data.json';

class KaryawanController {
  /* Releaase 0 - Register Karyawan */
  static register(req, res) {
    fs.readFile(path, (err, data) => {
      if (err) {
        res.status(400).json({
          errors: 'tidak bisa membaca data',
        });
      } else {
        let existingData = JSON.parse(data);
        let { users } = existingData;
        let { name, password, role } = req.body;
        let newKaryawan = { name, password, role, isLogin: false };
        users.push(newKaryawan);
        let newData = { ...existingData, users };

        fs.writeFile(path, JSON.stringify(newData), (err) => {
          if (err) {
            res.status(400).json({
              errors: 'tidak dapat menyimpan data',
            });
          } else {
            res.status(201).json({
              message: 'berhasil register',
            });
          }
        });
      }
    });
  }

  /* Release 1 - Show All Karyawan */
  static findAll(req, res) {
    fs.readFile(path, (err, data) => {
      if (err) {
        res.status(400).json({
          errors: 'tidak dapat membaca data',
        });
      } else {
        let realData = JSON.parse(data);
        res.status(200).json({
          message: 'berhasil membaca data',
          data: realData.users,
        });
      }
    });
  }

  /* Release 2 - Login */
  static login(req, res) {
    fsPromises
      .readFile(path)
      .then((data) => {
        let existingData = JSON.parse(data);
        let { users } = existingData;
        let { name, password } = req.body;
        let indexKr = users.findIndex((karyawan) => karyawan.name == name);

        if (indexKr == -1) {
          res.status(404).json({
            errors: 'data tidak ditemukan',
          });
        } else {
          let karyawan = users[indexKr];

          if (karyawan.password == password) {
            karyawan.isLogin = true;

            users.splice(indexKr, 1, karyawan);
            return fsPromises.writeFile(path, JSON.stringify(existingData));
          } else {
            res.status(400).json({
              errors: 'password salah',
            });
          }
        }
      })
      .then(() => {
        res.status(200).json({
          message: 'Berhasil login',
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }

  /* Release 3 - Add Siswa */
  static addStudent(req, res) {
    fsPromises
      .readFile(path)
      .then((data) => {
        let existingData = JSON.parse(data);
        let { users } = existingData;
        let { name, kelas } = req.body;
        let newStudent = { name, kelas };
        let newTrainerData = { students: [] };
        newTrainerData.students.push(newStudent);
        let indexKr = users.findIndex((karyawan) => karyawan.role == 'admin');
        let admin = users[indexKr];

        if (indexKr == -1) {
          res.status(404).json({
            errors: 'admin tidak ditemukan'
          })
        } else {
          if (admin.role == 'admin' && admin.isLogin == true) {
            let indexTr = users.findIndex((trainer) => trainer.name === req.params.name);
            let trainer = users[indexTr];
            let newTrainer = { ...trainer, newTrainerData };

            users.splice(indexTr, 1, trainer);

            return fsPromises.writeFile(path, JSON.stringify(newTrainer));
          }
        }
      })
      .then(() => {
        res.status(201).json({
          message: 'berhasil add siswa'
        })
      })
      .catch((err) => {
        console.log(err);
      });
  }
}

module.exports = KaryawanController;
