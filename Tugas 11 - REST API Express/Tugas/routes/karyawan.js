var express = require('express');
const { route } = require('.');
var router = express.Router();

/* Controller */
const KaryawanController = require('../controllers/karyawan');

/* GET
 * Show all karyawan
 */
router.get('/', KaryawanController.findAll);

/* POST
 * register karyawan
 * login
 * Add siswa
 */
router.post('/register', KaryawanController.register);
router.post('/login', KaryawanController.login);
router.post('/:name/siswa', KaryawanController.addStudent);

module.exports = router;
