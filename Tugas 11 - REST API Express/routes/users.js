var express = require('express');
const { route } = require('.');
var router = express.Router();

/* Controller */
const UserController = require('../controllers/users');

/* GET users listing. */
router.get('/', UserController.findAll);

router.get('/:id', (req, res) => {
  console.log('parameters: ', req.params);
  console.log('parameter id: ', req.params.id);
  res.send(`hello ${req.params.id} user`);
});

router.post('/register', UserController.register);

module.exports = router;
