function barisBaru() {
  console.log('');
}

// Soal 1 if-else
console.log('Soal 1');
var nama = 'Aldi';
var role = 'werewolf';

if (nama == '' && role == '') {
  console.log('Nama harus diisi!');
} else if (nama != '') {
  if (role == '') {
    console.log(`Halo ${nama}, Pilih peranmu untuk memulai game!`);
  } else if (role == 'Penyihir' || role == 'penyihir') {
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
    console.log(`Halo ${role} ${nama}, kamu dapat melihat siapa yang menjadi werewolf!`);
  } else if (role == 'Guard' || role == 'guard') {
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
    console.log(`Halo ${role} ${nama}, kamu akan membantu melindungi temanmu dari serangan werewolf`);
  } else if (role == 'Werewolf' || role == 'werewolf') {
    console.log(`Selamat datang di Dunia Werewolf, ${nama}`);
    console.log(`Halo ${role} ${nama}, kamu akan memakan mangsa setiap malam!`);
  } else {
    console.log(`Halo ${nama}, role tidak ditemukan`);
  }
}
barisBaru();

// Soal 2 switch case
console.log('Soal 2');
var hari = 21;
var bulan = 12;
var tahun = 1945;

switch (bulan) {
  case 1:
    console.log(`${hari} Januari ${tahun}`);
    break;
  case 2:
    console.log(`${hari} Februari ${tahun}`);
    break;
  case 3:
    console.log(`${hari} Maret ${tahun}`);
    break;
  case 4:
    console.log(`${hari} April ${tahun}`);
    break;
  case 5:
    console.log(`${hari} Mei ${tahun}`);
    break;
  case 6:
    console.log(`${hari} Juni ${tahun}`);
    break;
  case 7:
    console.log(`${hari} Juli ${tahun}`);
    break;
  case 8:
    console.log(`${hari} Agustus ${tahun}`);
    break;
  case 9:
    console.log(`${hari} September ${tahun}`);
    break;
  case 10:
    console.log(`${hari} Oktober ${tahun}`);
    break;
  case 11:
    console.log(`${hari} November ${tahun}`);
    break;
  case 12:
    console.log(`${hari} Desember ${tahun}`);
    break;
  default:
    console.log('Masukan angka bulan dari 1 - 12, KARENA 1 TAHUN 12 BULAN GAN!!');
    break;
}
barisBaru();
