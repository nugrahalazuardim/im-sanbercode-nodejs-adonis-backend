barisBaru = () => {
  console.log('');
};

console.log('Soal 1 - Array Multidimensi');
var input = [
  ['0001', 'Roman Alamsyah', 'Bandar Lampung', '21/05/1989', 'Membaca'],
  ['0002', 'Dika Sembiring', 'Medan', '10/10/1992', 'Bermain Gitar'],
  ['0003', 'Winona', 'Ambon', '25/12/1965', 'Memasak'],
  ['0004', 'Bintang Senjaya', 'Martapura', '6/4/1970', 'Berkebun'],
];

function dataHandling(array) {
  for (var l = 0; l < array.length; l++) {
    console.log(`Nomor ID: ${array[l][0]}`);
    console.log(`Nama Lengkap: ${array[l][1]}`);
    console.log(`TTL: ${array[l][2]} ${array[l][3]}`);
    console.log(`Hobi: ${array[l][4]}`);
    console.log('');
  }
}

dataHandling(input);
barisBaru();

console.log('Soal 2 - Balik Kata');
function balikKata(string) {
  var output = '';
  var panjangString = string.length;

  for (var h = panjangString - 1; h >= 0; h--) {
    output += string[h];
  }
  console.log(output);
}

balikKata('SanberCode');
balikKata('racecar');
balikKata('kasur rusak');
balikKata('haji ijah');
balikKata('I am Sanbers');
