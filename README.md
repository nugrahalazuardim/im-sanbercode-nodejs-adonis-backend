**Daftar belajar kelas Node.JS Sanbercode**

<table>
    <tr>
        <th>Materi - Pekan</th>
        <th>Link commit</th>
    </tr>
    <tr>
        <td>Materi 1 - Pekan 1</td>
        <td><b>[Berlatih Git](https://gitlab.com/nugrahalazuardim/im-sanbercode-nodejs-adonis-backend/-/commit/ff97321495b3ca9cb36a06da10d4b6d80f8536d6)</b></td>
    </tr>
    <tr>
        <td>Materi 2 - Pekan 1</td>
        <td><b>[Berlatih Variabel, String, dan Control Flow](https://gitlab.com/nugrahalazuardim/im-sanbercode-nodejs-adonis-backend/-/commit/202cb0b481006259176e46c0013ea48026cf3478)</b></td>
    </tr>
    <tr>
        <td>Materi 3 - Pekan 1</td>
        <td><b>[Berlatih Looping dan Functions](https://gitlab.com/nugrahalazuardim/im-sanbercode-nodejs-adonis-backend/-/commit/260a4f8a78781b1bc33e172526f757746884c684)</b></td>
    </tr>
    <tr>
        <td>Materi 4 - Pekan 1</td>
        <td><b>[Berlatih Array](https://gitlab.com/nugrahalazuardim/im-sanbercode-nodejs-adonis-backend/-/commit/b958316612608c8c95581a8756e0df707a1717a7)</b></td>
    </tr>
    <tr>
        <td>Materi 5 - Pekan 1</td>
        <td><b>[Berlatih Object Literal](https://gitlab.com/nugrahalazuardim/im-sanbercode-nodejs-adonis-backend/-/commit/74647f09ad617d400a96500cea76b8d631aa3be5)</b></td>
    </tr>
    <tr>
        <td>Quiz - Pekan 1</td>
        <td><b>[Quiz Pekan 1](https://gitlab.com/nugrahalazuardim/im-sanbercode-nodejs-adonis-backend/-/commit/d77319c4be6de258348fcc5eb6a8f33a24897ace)</b></td>
    </tr>
    <tr>
        <td>Materi 1 - Pekan 2</td>
        <td><b>[Berlatih ES6](https://gitlab.com/nugrahalazuardim/im-sanbercode-nodejs-adonis-backend/-/commit/e255a6c91207c95c3f146dd39fdf406fa12c35e8)</b></td>
    </tr>
    <tr>
        <td>Materi 2 - Pekan 2</td>
        <td><b>[Berlatih Class](https://gitlab.com/nugrahalazuardim/im-sanbercode-nodejs-adonis-backend/-/commit/6e1918ddd60e2f8ba27ff5c197af71070ba2456b)</b></td>
    </tr>
    <tr>
        <td>Materi 3 - Pekan 2</td>
        <td><b>[Berlatih Asynchronous](https://gitlab.com/nugrahalazuardim/im-sanbercode-nodejs-adonis-backend/-/commit/f12da6d2e5c853c34387c876132b8245d6c56796)</b></td>
    </tr>
    <tr>
        <td>Materi 4 - Pekan 2</td>
        <td><b>[Berlatih SQL](https://gitlab.com/nugrahalazuardim/im-sanbercode-nodejs-adonis-backend/-/commit/d521ddadffe60a12931669abb2134aeffcf16498)</b></td>
    </tr>
    <tr>
        <td>Materi 5 - Pekan 2</td>
        <td><b>[Berlatih ERD](https://gitlab.com/nugrahalazuardim/im-sanbercode-nodejs-adonis-backend/-/commit/cf9e021fadf970417e2eb18268f2326084f195ca)</b></td>
    </tr>
    <tr>
        <td>Quiz - Pekan 2</td>
        <td><b>[Quiz Pekan 2](https://gitlab.com/nugrahalazuardim/im-sanbercode-nodejs-adonis-backend/-/commit/78cc68c2588e5d7ef91d514acd7a4e728b0ca226)</b></td>
    </tr>
    <tr>
        <td>Materi 1 - Pekan 3</td>
        <td><b>[Berlatih RESTful API](https://gitlab.com/nugrahalazuardim/im-sanbercode-nodejs-adonis-backend/-/commit/03ddc3b073fc18f02270b3f54442384b28b0876c)</b></td>
    </tr>
</table>

> untuk melakukan editing markdown yang ada, baca **[dokumentasi](https://daringfireball.net/projects/markdown/syntax)** resminya
