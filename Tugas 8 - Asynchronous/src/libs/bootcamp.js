import Trainer from './trainer';
import fs from 'fs';
import 'core-js/stable';
import fsPromises from 'fs/promises';
const path = 'data.json';

export default class Bootcamp {
  constructor(name) {
    this.name = name;
  }

  get name() {
    return this._name;
  }

  set name(name) {
    this._name = name;
  }

  register(input) {
    let [name, password, role] = input.split(',');
    fs.readFile(path, (err, data) => {
      if (err) {
        console.log(err);
      }

      let existingData = JSON.parse(data); //* data dirubah menjadi JSON dari array menggunakan JSON.parse
      let employee = new Trainer(name, password, role);
      existingData.push(employee);
      fs.writeFile(path, JSON.stringify(existingData), (err) => {
        if (err) {
          console.log(err);
        } else {
          console.log('Berhasil register');
        }
      }); //* data yang telah dirubah ditulis ulang ke file tujuan sebagai JSON
    });
  }

  login(input) {
    let [name, password] = input.split(',');

    fsPromises
      .readFile(path)
      .then((data) => {
        let employees = JSON.parse(data);

        let indexTrn = employees.findIndex((trn) => trn._name == name);
        if (indexTrn == -1) {
          console.log('Data tidak ditemukan');
        } else {
          let employee = employees[indexTrn];

          if (employee._password == password) {
            employee._isLogin = true;

            employees.splice(indexTrn, 1, employee);
            return fsPromises.writeFile(path, JSON.stringify(employees));
          } else {
            console.log('Password salah, silahkan coba lagi');
          }
        }
      })
      .then(() => {
        console.log('Berhasil login');
      })
      .catch((err) => {
        console.log(err);
      });
  }

  async addSiswa(input) {
    let [name, trainer] = input.split(',');
    let readFile = await fsPromises.readFile(path);
    let existingData = JSON.parse(readFile);

    let indexAd = existingData.findIndex((Trainer) => Trainer._role == 'admin');
    let Admin = existingData[indexAd];

    if (indexAd == -1) {
      console.log('User tidak ditemukan');
    } else if (Admin._role == 'admin' && Admin._isLogin == true) {
      let indexTr = existingData.findIndex((Trainer) => Trainer._name == trainer);
      let newTrainer = existingData[indexTr];

      newTrainer._students.push({ name });
      existingData.splice(indexTr, 1, newTrainer);

      await fsPromises.writeFile(path, JSON.stringify(existingData));
      console.log('Berhasil add siswa');
    } else {
      console.log('Admin tidak login');
    }
  }
}
