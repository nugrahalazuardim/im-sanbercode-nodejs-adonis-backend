class Trainer {
  constructor(name, password, role) {
    (this._name = name), (this._password = password), (this._role = role), (this._isLogin = false), (this._students = []);
  }

  get name() {
    return this._name;
  }

  set name(strName) {
    this._name = strName;
  }

  get password() {
    return this._password;
  }

  set password(strPassword) {
    this._password = strPassword;
  }

  get role() {
    return this._role;
  }

  set role(strRole) {
    this._role = strRole;
  }

  get isLogin() {
    return this._isLogin;
  }

  set isLogin(status) {
    this._isLogin = status;
  }

  get students() {
    return this._students;
  }

  set students(students) {
    this._students = students;
  }
}

export default Trainer;
