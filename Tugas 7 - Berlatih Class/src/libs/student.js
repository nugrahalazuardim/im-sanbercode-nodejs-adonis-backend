class Student {
  constructor(name) {
    this._name = name;
    this._score = [];
  }

  get studentName() {
    return this._name;
  }

  set studentName(strStudent) {
    this._name = strStudent;
  }
}

export default Student;
