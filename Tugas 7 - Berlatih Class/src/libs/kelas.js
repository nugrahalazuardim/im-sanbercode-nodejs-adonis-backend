class Kelas {
  constructor(name, level, instructor) {
    (this._name = name), (this._students = []), (this._level = level), (this._instructor = instructor);
  }

  get className() {
    return this._name;
  }

  set className(strClass) {
    this._name = strClass;
  }

  get classLevel() {
    return this._level;
  }

  set classLevel(strLevel) {
    this._level = strLevel;
  }

  get classInstructor() {
    return this._instructor;
  }

  set classInstructor(strInstructor) {
    this._instructor = strInstructor;
  }

  get students() {
    return this._students;
  }

  addStudent(objStudent) {
    this._students.push(objStudent);
  }
}

export default Kelas;
