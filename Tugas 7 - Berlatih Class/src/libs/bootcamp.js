import Kelas from './kelas';

class Bootcamp {
  constructor(name) {
    this._name = name;
    this._classes = [];
  }

  get bootcampName() {
    return this._name;
  }

  set bootcampName(strBootcamp) {
    this._name = strBootcamp;
  }

  get classes() {
    return this._classes;
  }

  createClass(name, level, instructor) {
    let newClass = new Kelas(name, level, instructor);
    this._classes.push(newClass);
  }

  insertStudent(namaKelas, objStudent) {
    let findClass = this._classes.find((kelas) => kelas.className == namaKelas);
    findClass.addStudent(objStudent);
  }
}

export default Bootcamp;
