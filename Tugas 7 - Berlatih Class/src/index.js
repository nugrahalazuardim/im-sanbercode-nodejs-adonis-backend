import Bootcamp from './libs/bootcamp';
import Student from './libs/student';

const sanber = new Bootcamp('sanbercode');

sanber.createClass('Laravel', 'beginner', 'abduh');
sanber.createClass('React', 'beginner', 'abdul');

let studentsList = ['regi', 'ahmad', 'bondra', 'iqbal', 'putri', 'rezky'];

studentsList.map((students, index) => {
  let newStudent = new Student(students);
  let kelas = sanber.classes[index % 2].className;
  sanber.insertStudent(kelas, newStudent);
});

sanber.classes.forEach((kelas) => {
  console.log(kelas);
});
