barisBaru = () => {
  console.log('');
};

console.log('Soal 1 - Array to Object');
function arrayToObject(array) {
  var people = {};
  var now = new Date();
  for (var p = 0; p < array.length; p++) {
    var thisYear = now.getFullYear();
    var countAge = thisYear - array[p][3];

    people = {
      firstName: array[p][0],
      lastName: array[p][1],
      gender: array[p][2],
      age: countAge,
    };

    if (array[p][3] === undefined || array[p][3] > thisYear) {
      people.age = 'Invalid birth year';
    }

    var fullName = `${people.firstName} ${people.lastName}`;
    console.log(`${p + 1}. ${fullName}: `, people);
  }
}

var people = [
  ['Bruce', 'Banner', 'male', 1975],
  ['Natasha', 'Romanoff', 'female'],
];

var people2 = [
  ['Tony', 'Stark', 'male', 1980],
  ['Pepper', 'Pots', 'female', 2023],
];

arrayToObject(people);
arrayToObject(people2);
barisBaru();

console.log('Soal 2 - Naik Angkot');
function naikAngkot(listPenumpang) {
  rute = ['A', 'B', 'C', 'D', 'E', 'F'];
  var arrayPenumpang = [];
  for (var p = 0; p < listPenumpang.length; p++) {
    var bayar = 0;
    var dataPenumpang = {};

    dataPenumpang = {
      penumpang: listPenumpang[p][0],
      naikDari: listPenumpang[p][1],
      tujuan: listPenumpang[p][2],
    };

    var bayar = (rute.indexOf(listPenumpang[p][2]) - rute.indexOf(listPenumpang[p][1])) * 2000;

    dataPenumpang.bayar = bayar;
    arrayPenumpang.push(dataPenumpang);
  }
  return arrayPenumpang;
}

var penumpang = [
  ['Dimitri', 'B', 'F'],
  ['Icha', 'A', 'B'],
];

console.log(naikAngkot(penumpang));
barisBaru();

console.log('Soal 3 - Nilai Tertinggi');
function nilaiTertinggi(siswa) {
  var hasil = {};
  for (var p = 0; p < siswa.length; p++) {
    var data = siswa[p];
    if (!hasil[data.class]) {
      hasil[data.class] = {
        name: data.name,
        score: data.score,
      };
    } else {
      if (data.score > hasil[data.class].score) {
        hasil[data.class] = {
          name: data.name,
          score: data.score,
        };
      }
    }
  }
  return hasil;
}

var peserta1 = [
  { name: 'Asep', score: 90, class: 'adonis' },
  { name: 'Ahmad', score: 85, class: 'vuejs' },
  { name: 'Regi', score: 74, class: 'adonis' },
  { name: 'Afrida', score: 78, class: 'reactjs' },
];

var peserta2 = [
  { name: 'Bondra', score: 100, class: 'adonis' },
  { name: 'Putri', score: 76, class: 'laravel' },
  { name: 'Iqbal', score: 96, class: 'adonis' },
  { name: 'Tyar', score: 71, class: 'laravel' },
  { name: 'Hilmy', score: 80, class: 'vuejs' },
];

console.log(nilaiTertinggi(peserta1));
