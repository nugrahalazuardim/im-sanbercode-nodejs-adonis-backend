// Soal 1 - Function Sapa
export const sapa = (str) => {
  return `halo selamat pagi, ${str}`;
};

// Soal 2 - Konversi Object
export const convert = (nama, domisili, umur) => {
  return {
    nama,
    domisili,
    umur,
  };
};

// Soal 3 - Check Score
export const checkScore = (arr) => {
  const pisah = arr.split(',');
  let value = [];

  for (var p = 0; p < pisah.length; p++) {
    const kata = pisah[p].split(':')[1];
    value.push(kata);
  }

  const [name, kelas, score] = value;
  return { name, kelas, score };
};

const data = [
  { name: 'Ahmad', kelas: 'adonis' },
  { name: 'Regi', kelas: 'laravel' },
  { name: 'Bondra', kelas: 'adonis' },
  { name: 'Iqbal', kelas: 'vuejs' },
  { name: 'Putri', kelas: 'Laravel' },
];

// Soal 4 - Filter Data
export const filterData = (kelas) => {
  for (var k = 0; k < data.length; k++) {
    return data.filter((el) => el['kelas'].toLowerCase().includes(kelas.toLowerCase()));
  }
};
