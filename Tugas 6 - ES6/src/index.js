import { sapa, convert, filterData, checkScore } from './lib/soal';

const myArgs = process.argv.slice(2);
const command = myArgs[0];

switch (command) {
  case 'sapa':
    let str = myArgs[1];
    console.log(sapa(str));
    break;
  case 'convert':
    const params = myArgs.slice(1);
    let [nama, domisili, umur] = params;
    console.log(convert(nama, domisili, umur));
    break;
  case 'checkScore':
    let data = myArgs[1];
    console.log(checkScore(data));
    break;
  case 'filterData':
    let namaKelas = myArgs[1];
    console.log(filterData(namaKelas));
    break;
  default:
    break;
}
