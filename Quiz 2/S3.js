class OlahString {
  static check_palindrome(strPL) {
    let reverse = '';
    for (let p = strPL.length - 1; p >= 0; p--) {
      reverse += strPL[p];
    }
    return reverse;
  }

  static palindrome(string) {
    let reverse_str = this.check_palindrome(string);

    if (reverse_str === string) {
      console.log(true);
    } else {
      console.log(false);
    }
  }

  static ubahKapital(strKL) {
    let strCapital = strKL
      .split(' ')
      .map((nama) => nama.charAt(0).toUpperCase() + nama.slice(1))
      .join(' ');

    console.log(strCapital);
  }
}

OlahString.palindrome('katak');
OlahString.palindrome('sanbers');
OlahString.ubahKapital('asep');
OlahString.ubahKapital('abduh');
